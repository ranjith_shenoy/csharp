﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Domain
{
    public class Book
    {
        public string Title { get; set; }

        public string Author { get; set; }
    }
}
