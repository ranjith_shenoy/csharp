﻿Feature: Library
	In order to keep track of my books
	I want a system to do that for me
	And show me all the books I own

@mytag
Scenario: Add Book
	Given I have book with title "Harry Potter"
	And Author is "J.K"
	When I add the book to the library
	Then my library should have the added book '{"Title":"Harry Potter","Author":"J.K"}'

@getBook
Scenario: List Books in Library
	Given I have a library of books
	When I fetch all the books
	Then my list should be like
	| Title        | Author    |
	| Harry Potter | J.K       |
	| CSharp       | Jon Skeet |
