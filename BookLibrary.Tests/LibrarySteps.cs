﻿using BookLibrary.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;

namespace BookLibrary.Tests
{
    [Binding]
    public class LibrarySteps
    {
        Book book = new Book();
        string bookTitle = "";
        private LibraryService _service = new LibraryService();
        private readonly ScenarioContext scenarioContext;

        public LibrarySteps(ScenarioContext context)
        {
            scenarioContext = context;
        }

        [Given(@"I have book with title ""(.*)""")]
        public void GivenIHaveBookWithTitle(string title)
        {
            book.Title = title;
            bookTitle = title;
        }
        
        [Given(@"Author is ""(.*)""")]
        public void GivenAuthorIs(string author)
        {
            book.Author = author;
        }
        
        [When(@"I add the book to the library")]
        public void WhenIAddTheBookToTheLibrary()
        {
            _service.AddBook(book);
        }

        [Then(@"my library should have the added book '(.*)'")]
        public void ThenMyLibraryShouldHaveTheAddedBook(string expected)
        {
            var book = _service.GetBook(bookTitle);
            var jsonResult = JsonConvert.SerializeObject(book);
            Assert.Equal(expected, jsonResult);
        }

        [Given(@"I have a library of books")]
        public void GivenIHaveALibraryOfBooks()
        {
        }

        [When(@"I fetch all the books")]
        public void WhenIFetchAllTheBooks()
        {
            var books = _service.GetBooks();
            scenarioContext.Add("books", books);
        }

        [Then(@"my list should be like")]
        public void ThenMyListShouldBeLike(Table table)
        {
            var books = (List<Book>)scenarioContext["books"];
            table.CompareToSet(books);
        }

        [BeforeScenario("getBook")]
        public void AddTestData()
        {
            _service.GetBooks().Clear();
            _service.AddBook(new Book()
            {
                Title = "Harry Potter",
                Author = "J.K"
            });
            _service.AddBook(new Book()
            {
                Title = "CSharp",
                Author = "Jon Skeet"
            });
        }
    }
}
