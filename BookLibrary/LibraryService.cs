﻿using BookLibrary.Domain;
using BookLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary
{
    public class LibraryService
    {
        private BookRepository _bookRepository = new BookRepository();

        public void AddBook(Book newBook)
        {
            if (string.IsNullOrEmpty(newBook.Title))
            {
                throw new Exception("Title can't be empty");
            }

            _bookRepository.Add(newBook);
        }

        public List<Book> GetBooks()
        {
            return _bookRepository.GetBooks();
        }

        public Book GetBook(string title)
        {
            var book = _bookRepository.GetBook(title);
            if(book == null)
            {
                throw new Exception("No book found");
            }
            return book;
        }
    }
}
