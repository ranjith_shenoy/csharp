﻿using System.Collections.Generic;
using BookLibrary.Domain;
using System.Linq;

namespace BookLibrary.Repository
{
    public class BookRepository
    {
        private List<Book> _books = new List<Book>();

        public void Add(Book newBook)
        {
            _books.Add(newBook);
        }

        public List<Book> GetBooks()
        {
            return _books;
        }

        public Book GetBook(string title)
        {
            return _books.FirstOrDefault(b => b.Title == title);
        }
    }
}
